package com.jongsoft.language.fasterxml;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jongsoft.lang.API;
import com.jongsoft.lang.Collections;
import com.jongsoft.lang.collection.Sequence;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class SequenceTest {

    public static final String COMPLEX_TYPE = "[{\"name\":\"test\",\"age\":1,\"child\":{\"name\":\"test-child\",\"age\":2,\"child\":null}},{\"name\":\"test-2\",\"age\":4,\"child\":{\"name\":\"test-2-child\",\"age\":1,\"child\":null}}]";
    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new LanguageModule());
    }

    @Test
    public void serializeSimpleType() throws JsonProcessingException {
        String value = objectMapper.writeValueAsString(Collections.List("one", "two", "three"));
        assertThat(value, equalTo("[\"one\",\"two\",\"three\"]"));

        value = objectMapper.writeValueAsString(Collections.List(1, 2, 3, 4));
        assertThat(value, equalTo("[1,2,3,4]"));

        value = objectMapper.writeValueAsString(Collections.List(true, false, true));
        assertThat(value, equalTo("[true,false,true]"));
    }

    @Test
    public void serializeComplexType() throws JsonProcessingException {
        Sequence<SimpleObject> list = Collections.List(
            new SimpleObject("test", 1, new SimpleObject("test-child", 2, null)),
            new SimpleObject("test-2", 4, new SimpleObject("test-2-child", 1, null))
        );

        String value = objectMapper.writeValueAsString(list);
        assertThat(value, equalTo(COMPLEX_TYPE));
    }

    @Test
    public void deserializeSimpleType() throws IOException {
        Sequence<Integer> parsed = objectMapper.readValue("[1,2,3,4]", new TypeReference<Sequence<Integer>>() {});

        assertThat(parsed, notNullValue());
        assertThat(parsed.size(), equalTo(4));
        assertThat(parsed, hasItems(1,2,3,4));
    }

    @Test
    public void deserializeComplexType() throws IOException {
        Sequence<SimpleObject> parsed = objectMapper.readValue(COMPLEX_TYPE, new TypeReference<Sequence<SimpleObject>>() {});

        assertThat(parsed.size(), equalTo(2));
        assertThat(parsed.get(0).getName(), equalTo("test"));
        assertThat(parsed.get(0).getAge(), equalTo(1));
        assertThat(parsed.get(1).getName(), equalTo("test-2"));
        assertThat(parsed.get(1).getAge(), equalTo(4));
    }
}
