package com.jongsoft.language.fasterxml;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jongsoft.lang.API;
import com.jongsoft.lang.Control;
import com.jongsoft.lang.control.Optional;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class OptionalTest {

    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new LanguageModule());

    }

    @Test
    public void serializeSimpleType() throws JsonProcessingException {
        String value = objectMapper.writeValueAsString(Control.Option(null));
        assertThat(value, equalTo("null"));

        value = objectMapper.writeValueAsString(Control.Option(100.3));
        assertThat(value, equalTo("100.3"));
    }

    @Test
    public void deserializeSimpleType() throws IOException {
        Optional<String> result = objectMapper.readValue("null", new TypeReference<Optional<String>>(){});
        assertThat(result.isPresent(), equalTo(false));

        Optional<String> valid = objectMapper.readValue("\"test\"", new TypeReference<Optional<String>>(){});
        assertThat(valid.isPresent(), equalTo(true));
        assertThat(valid.get(), equalTo("test"));
    }
}
