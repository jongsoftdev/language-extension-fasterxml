package com.jongsoft.language.fasterxml;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jongsoft.lang.API;
import com.jongsoft.lang.Collections;
import com.jongsoft.lang.collection.Map;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class MapTest {

    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new LanguageModule());
    }

    @Test
    public void simpleMapSerialize() throws JsonProcessingException {
        Map<String, Object> map = Collections.<String, Object>Map("prop1", 1)
                .put("prop2", new SimpleObject("auto", 1, null))
                .put("prop3", "two");

        String value = objectMapper.writeValueAsString(map);
        assertThat(value, equalTo("{\"prop1\":1,\"prop2\":{\"name\":\"auto\",\"age\":1,\"child\":null},\"prop3\":\"two\"}"));
    }

}
