package com.jongsoft.language.fasterxml;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jongsoft.lang.Collections;
import com.jongsoft.lang.collection.Iterator;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class IteratorTest {

    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new LanguageModule());
    }

    @Test
    public void serializeSimple() throws JsonProcessingException {
        String value = objectMapper.writeValueAsString(Collections.Iterator(1, 2, 3, 4));
        assertThat(value, equalTo("[1,2,3,4]"));
    }

    @Test
    public void deserializerSimple() throws IOException {
        Iterator<Integer> result = objectMapper.readValue("[1,2,3,4]", new TypeReference<Iterator<Integer>>(){});

        assertThat(result.next(), equalTo(1));
        assertThat(result.next(), equalTo(2));
        assertThat(result.next(), equalTo(3));
        assertThat(result.next(), equalTo(4));
        assertThat(result.hasNext(), equalTo(false));
    }
}
