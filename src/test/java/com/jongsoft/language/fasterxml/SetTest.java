package com.jongsoft.language.fasterxml;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jongsoft.lang.API;
import com.jongsoft.lang.Collections;
import com.jongsoft.lang.collection.Sequence;
import com.jongsoft.lang.collection.Set;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

public class SetTest {
    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new LanguageModule());
    }

    @Test
    public void simpleSerialize() throws JsonProcessingException {
        String value = objectMapper.writeValueAsString(Collections.Set(1, 2, 3, 4));
        assertThat(value, equalTo("[1,2,3,4]"));
    }

    @Test
    public void simpleDeserialize() throws IOException {
        Set<Integer> result = objectMapper.readValue("[1,2,3,4]", new TypeReference<Set<Integer>>() {});

        assertThat(result.size(), equalTo(4));
        assertThat(result, hasItems(1, 2, 3, 4));
    }

    @Test
    public void deserializeComplexType() throws IOException {
        Sequence<SimpleObject> parsed = objectMapper.readValue(SequenceTest.COMPLEX_TYPE, new TypeReference<Sequence<SimpleObject>>() {});

        assertThat(parsed.size(), equalTo(2));
        assertThat(parsed.get(0).getName(), equalTo("test"));
        assertThat(parsed.get(0).getAge(), equalTo(1));
        assertThat(parsed.get(1).getName(), equalTo("test-2"));
        assertThat(parsed.get(1).getAge(), equalTo(4));
    }

}
