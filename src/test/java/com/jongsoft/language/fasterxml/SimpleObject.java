package com.jongsoft.language.fasterxml;

public class SimpleObject {

    private String name;
    private int age;
    private SimpleObject child;

    SimpleObject() {
        // needed for deserialization
    }

    public SimpleObject(String name, int age, SimpleObject child) {
        this.name = name;
        this.age = age;
        this.child = child;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public SimpleObject getChild() {
        return child;
    }

}
