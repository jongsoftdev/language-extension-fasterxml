module com.jongsoft.lang.fasterxml {
    requires com.jongsoft.lang;

    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.core;

    exports com.jongsoft.language.fasterxml;
}