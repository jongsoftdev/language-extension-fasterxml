/**
 * This library adds support for FasterXml to the language library.
 *
 * Currently this library exposes a {@link com.fasterxml.jackson.databind.Module} that can be added to the {@link com.fasterxml.jackson.databind.ObjectMapper}
 * to add serialization and deserialization support.
 * <p>
 *     Below is an example of how to enable the module in any existing {@linkplain com.fasterxml.jackson.databind.ObjectMapper}.
 * </p>
 * <pre>{@code
 *    ObjectMapper mapper = new ObjectMapper();
 *    mapper.registerModule(new LanguageModule());
 * }</pre>
 *
 * <p>With this version of the library the following interfaces are supported using FasterXml</p>
 * <table>
 *     <caption>Data description</caption>
 *     <thead>
 *         <tr>
 *             <th>Interface</th>
 *             <th>Serialization</th>
 *             <th>Deserialization</th>
 *         </tr>
 *     </thead>
 *     <tbody>
 *         <tr><td>{@linkplain com.jongsoft.lang.collection.Sequence}</td><td>Yes</td><td>Yes</td></tr>
 *         <tr><td>{@linkplain com.jongsoft.lang.collection.Set}</td><td>Yes</td><td>Yes</td></tr>
 *         <tr><td>{@linkplain com.jongsoft.lang.collection.Map}</td><td>Yes</td><td>No</td></tr>
 *         <tr><td>{@linkplain com.jongsoft.lang.control.Optional}</td><td>Yes</td><td>Yes</td></tr>
 *     </tbody>
 * </table>
 */
package com.jongsoft.language.fasterxml;
