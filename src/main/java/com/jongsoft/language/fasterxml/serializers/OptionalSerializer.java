package com.jongsoft.language.fasterxml.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.jongsoft.lang.Control;
import com.jongsoft.lang.control.Optional;

import java.io.IOException;

public class OptionalSerializer extends JsonSerializer<Optional> {

    @Override
    public void serialize(Optional optional, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        ((Optional<?>) optional)
                .ifPresent(e -> Control.Try(() -> jsonGenerator.writeObject(e)))
                .elseRun(() -> Control.Try(jsonGenerator::writeNull));
    }

    @Override
    public Class<Optional> handledType() {
        return Optional.class;
    }

}
