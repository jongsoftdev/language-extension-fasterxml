package com.jongsoft.language.fasterxml.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.jongsoft.lang.API;
import com.jongsoft.lang.collection.Map;
import com.jongsoft.lang.collection.tuple.Pair;
import com.jongsoft.lang.collection.tuple.Tuple;

import java.io.IOException;

public class MapSerializer extends JsonSerializer<Map> {

    @Override
    public void serialize(Map value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();

        Map<?, ?> looper = value;
        while (looper.size() > 0) {
            Pair head = looper.head();
            gen.writeObjectField(head.getFirst().toString(), head.getSecond());
            looper = looper.tail();
        }

        gen.writeEndObject();
    }

    @Override
    public Class<Map> handledType() {
        return Map.class;
    }

}
