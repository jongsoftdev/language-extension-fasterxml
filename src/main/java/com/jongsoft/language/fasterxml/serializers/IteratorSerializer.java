package com.jongsoft.language.fasterxml.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.jongsoft.lang.Value;

import java.io.IOException;

public class IteratorSerializer extends JsonSerializer<Value> {

    @Override
    public void serialize(Value value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartArray();

        for (Object o : value) {
            gen.writeObject(o);
        }

        gen.writeEndArray();
    }

    @Override
    public Class<Value> handledType() {
        return Value.class;
    }

}
