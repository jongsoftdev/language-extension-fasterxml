package com.jongsoft.language.fasterxml.deserializers;

import com.jongsoft.lang.Collections;
import com.jongsoft.lang.collection.Iterator;

public class IteratorDeserializer extends CollectionDeserializer<Iterator> {

    @Override
    Iterator createCollection(Object[] content) {
        return Collections.Iterator(content);
    }

    @Override
    CollectionDeserializer<Iterator> createDeserializer() {
        return new IteratorDeserializer();
    }

}
