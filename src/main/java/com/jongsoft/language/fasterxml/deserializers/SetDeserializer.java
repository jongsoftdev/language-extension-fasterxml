package com.jongsoft.language.fasterxml.deserializers;

import com.jongsoft.lang.API;
import com.jongsoft.lang.Collections;
import com.jongsoft.lang.collection.Set;

public class SetDeserializer extends CollectionDeserializer<Set> {

    @Override
    Set createCollection(Object[] content) {
        return Collections.Set(content);
    }

    @Override
    CollectionDeserializer<Set> createDeserializer() {
        return new SetDeserializer();
    }

}
