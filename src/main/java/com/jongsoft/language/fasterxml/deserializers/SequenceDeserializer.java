package com.jongsoft.language.fasterxml.deserializers;

import com.jongsoft.lang.Collections;
import com.jongsoft.lang.collection.Sequence;

public class SequenceDeserializer extends CollectionDeserializer<Sequence> {

    @Override
    Sequence createCollection(Object[] content) {
        return Collections.List(content);
    }

    @Override
    CollectionDeserializer<Sequence> createDeserializer() {
        return new SequenceDeserializer();
    }

}
