package com.jongsoft.language.fasterxml.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.jongsoft.lang.API;
import com.jongsoft.lang.Control;
import com.jongsoft.lang.control.Optional;

import java.io.IOException;

public class OptionalDeserializer extends JsonDeserializer<Optional> implements ContextualDeserializer {

    private JsonDeserializer valueDeserializer;

    @Override
    public Optional deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        if (!jsonParser.hasCurrentToken()) {
            return Control.Option(null);
        }

        return Control.Option(valueDeserializer.deserialize(jsonParser, deserializationContext));
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext deserializationContext, BeanProperty beanProperty) throws JsonMappingException {
        JavaType valueType = Control.Option(beanProperty)
                .map(BeanProperty::getType)
                .map(t -> t.containedType(0))
                .getOrSupply(() -> deserializationContext.getContextualType().containedType(0));

        OptionalDeserializer deserializer = new OptionalDeserializer();
        deserializer.valueDeserializer = deserializationContext.findRootValueDeserializer(valueType);
        return deserializer;
    }

    @Override
    public Optional getNullValue(DeserializationContext ctxt) throws JsonMappingException {
        return Control.Option();
    }
}
