package com.jongsoft.language.fasterxml.deserializers;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.util.ObjectBuffer;
import com.jongsoft.lang.Value;

import java.io.IOException;

public abstract class CollectionDeserializer<T extends Value> extends JsonDeserializer<T> implements ContextualDeserializer {

    private JsonDeserializer valueDeserializer;

    @Override
    public T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        if (!p.isExpectedStartArrayToken()) {
            throw new JsonParseException(p, "Expected ARRAY token but not found");
        }

        final ObjectBuffer buffer = ctxt.leaseObjectBuffer();

        Object[] chunks = buffer.resetAndStart();
        JsonToken token;
        int ix = 0;
        while ((token = p.nextToken()) != JsonToken.END_ARRAY) {
            Object value;

            if (token == JsonToken.VALUE_NULL) {
                value = null;
            } else {
                value = valueDeserializer.deserialize(p, ctxt);
            }

            if (ix >= chunks.length) {
                chunks = buffer.appendCompletedChunk(chunks);
                ix = 0;
            }

            chunks[ix++] = value;
        }

        Object[] rawValues = buffer.completeAndClearBuffer(chunks, ix);
        ctxt.returnObjectBuffer(buffer);
        return createCollection(rawValues);
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
        JavaType valueType;
        if (property == null) {
            valueType = ctxt.getContextualType().containedType(0);
        } else {
            valueType = property.getType().containedType(0);
        }

        CollectionDeserializer sequenceDeserializer = createDeserializer();
        sequenceDeserializer.valueDeserializer = ctxt.findNonContextualValueDeserializer(valueType);
        return sequenceDeserializer;
    }

    abstract T createCollection(Object[] content);
    abstract CollectionDeserializer<T> createDeserializer();

}
