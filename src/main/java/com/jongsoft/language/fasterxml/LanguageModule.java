package com.jongsoft.language.fasterxml;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.jongsoft.lang.collection.Iterator;
import com.jongsoft.lang.collection.Sequence;
import com.jongsoft.lang.collection.Set;
import com.jongsoft.lang.control.Optional;
import com.jongsoft.language.fasterxml.deserializers.OptionalDeserializer;
import com.jongsoft.language.fasterxml.deserializers.IteratorDeserializer;
import com.jongsoft.language.fasterxml.deserializers.SequenceDeserializer;
import com.jongsoft.language.fasterxml.deserializers.SetDeserializer;
import com.jongsoft.language.fasterxml.serializers.IteratorSerializer;
import com.jongsoft.language.fasterxml.serializers.MapSerializer;
import com.jongsoft.language.fasterxml.serializers.OptionalSerializer;

/**
 * The language module can be added to the {@linkplain com.fasterxml.jackson.databind.ObjectMapper#registerModule(Module)}
 * to add support for {@code language} interfaces from the Jong Soft Java Language library.
 */
public class LanguageModule extends SimpleModule {

    private static final Version VERSION = new Version(0, 0, 7, null, "com.jongsoft.language", "language-fasterxml");

    /**
     * The constructor will setup the module adding all required serializers and deserializers for the language module.
     */
    public LanguageModule() {
        super("language-module", VERSION);

        addSerializer(new IteratorSerializer());
        addSerializer(new MapSerializer());
        addSerializer(new OptionalSerializer());

        addDeserializer(Sequence.class, new SequenceDeserializer());
        addDeserializer(Set.class, new SetDeserializer());
        addDeserializer(Iterator.class, new IteratorDeserializer());
        addDeserializer(Optional.class, new OptionalDeserializer());
    }

}
