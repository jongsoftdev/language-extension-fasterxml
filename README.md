# Java Language Extensions: FasterXml module

[![Maven Central](https://img.shields.io/maven-central/v/com.jongsoft.lang/language-fasterxml.svg?color=green&style=flat-square)](https://search.maven.org/artifact/com.jongsoft.lang/language-fasterxml/)
[![SonarCloud Quality](https://sonarcloud.io/api/project_badges/measure?project=com.jongsoft.lang%3Alanguage-fasterxml&metric=alert_status&?style=flat-square)](https://sonarcloud.io/dashboard?id=com.jongsoft.lang%3Alanguage-fasterxml)
![CircleCI](https://img.shields.io/circleci/project/bitbucket/jongsoftdev/language-extension-fasterxml/master.svg?style=flat-square)
[![Coveralls bitbucket branch](https://img.shields.io/coveralls/bitbucket/jongsoftdev/language-extension-fasterxml/master.svg?style=flat-square)](https://coveralls.io/bitbucket/jongsoftdev/language-extension-fasterxml?branch=master)
![APMLicense](https://img.shields.io/apm/l/vim-mode.svg?style=flat-square)

This library adds support for FasterXml serialization and deserialization of the classes introduced in the 
[Java Language Extensions](https://jdocstorage.z6.web.core.windows.net/java/language/com.jongsoft.lang/module-summary.html)
library.

*NB:* Supporting version 0.0.5 of language library

## Supported interfaces

Currently the following interfaces are supported for FasterXml:

### Bidirectional
 
* `Sequence`
* `Set`

### Serialization only

* `Map`

## Documentation
To support development work the JavaDoc is published online for this library. Currently the following documentation is
published:
    
* [Production API Documentation](https://jdocstorage.z6.web.core.windows.net/java/language-fasterxml/com.jongsoft.lang/module-summary.html)
* [Snapshot API Documentation](https://jdocstorage.z6.web.core.windows.net/java/language-fasterxml/snapshot/index.html)

## Maven usage
To include this module use the Maven dependency below:

```xml
    <dependency>
        <groupId>com.jongsoft.lang</groupId>
        <artifactId>language-fasterxml</artifactId>
        <version>0.0.5</version>
    </dependency>
```

## License
The MIT License (MIT)
Copyright (c) 2016-2019 Jong Soft Development

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
  to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
